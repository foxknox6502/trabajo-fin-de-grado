#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""License:

Copyright (c) 2019 Ángel G.

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3 as published by the Free
Software Foundation.

This software makes use of PyQt5.

PyQt5 is dual licensed on all platforms under the Riverbank Commercial License
and the GPL v3. Your PyQt5 license must be compatible with your Qt license. If
you use the GPL version then your own code must also use a compatible license.

PyQt5, unlike Qt, is not available under the LGPL.

You can purchase a commercial PyQt5 license here:
<https://www.riverbankcomputing.com/commercial/buy>

Special thanks to David R. from Kron Technologies, for providing me examples
about how to use PyQt5 in real world applications.
"""

import sys
from collections import deque
from pickle import Pickler
from random import randint

import matplotlib.animation as animation
import numpy as np
from matplotlib.axes import Axes
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FingureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from PyQt5.QtCore import QSettings, Qt, QTimer, pyqtSignal, pyqtSlot
from PyQt5.QtSerialPort import QSerialPort
from PyQt5.QtWidgets import (
    QApplication,
    QDial,
    QDialog,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QLCDNumber,
    QLineEdit,
    QPushButton,
    QSpinBox,
    QTextEdit,
    QVBoxLayout,
    QMessageBox,
)

_LOAD_CELL_OFFSET = 56216  ## Do not edit this value ##


def adc_reading_to_mv(value):
    """adc_reading_to_mv Converts ADC reading to mV
    
    :param value: ADC reading
    :type value: int
    :return: Voltage reading in mV
    :rtype: string
    """
    _ADC_VREF = 4465  # [mV]
    _ADC_1LSB = _ADC_VREF / 2 ** 24
    return "{0:.4f}".format(value * _ADC_1LSB)


class Ui(QDialog):

    window_title = "TFG Ángel García 2018/19"
    default_serial_port = "/dev/ttyACM0"
    servo_pulse_width = 0
    settings_updated = True
    set_point_max = 300000
    set_point_min = 100000
    current_set_point = 0
    saved_data = []

    def __init__(self):
        super().__init__()
        self.setGeometry(0, 0, 1280, 720)
        self.setWindowTitle(self.window_title + " - " + "Disconnected")
        self.finished.connect(self.dialog_closed)

        # Persistent platform-independent application settings
        self.app_settings = QSettings(
            "tfg_app_settings"
        )  # /home/usuario/.config/tfg_app_settings.conf
        if "App/DefaultSerialPort" in self.app_settings.allKeys():
            self.default_serial_port = self.app_settings.value("App/DefaultSerialPort")
        else:
            self.app_settings.setValue(
                "App/DefaultSerialPort", self.default_serial_port
            )
        if "App/CurrentSetPoint" in self.app_settings.allKeys():
            self.current_set_point = int(self.app_settings.value("App/CurrentSetPoint"))
        else:
            self.current_set_point = 180000

        self.about_msg_box = QMessageBox()
        self.about_msg_box.setWindowTitle("About")
        self.about_msg_box.setText(
            "--- Trabajo Fin de Grado ---\n\n"
            + "Alumno:\tGarcía Troya, Ángel Manuel\n"
            + "Grado:\tIngeniería Electrónica Industrial\n"
            + "Tutores:\tLarios Marín, Diego Francisco\n"
            + "\tPersonal Vázquez, Enrique\n"
            + "Curso:\t2018/19"
        )

        main_window_layout = QHBoxLayout()
        v_box_layout_4 = QVBoxLayout()

        group_box_1 = QGroupBox("Serial Port Settings", self)
        group_box_1.setAlignment(Qt.AlignCenter)
        self.label_1 = QLabel("Device name:", self)
        self.device_name = QLineEdit(self.default_serial_port, self)
        self.connect_button = QPushButton("Connect", self)
        self.connect_button.setMinimumWidth(120)
        self.connect_button.clicked.connect(self.connect_button_clicked)
        self.disconnect_button = QPushButton("Disconnect", self)
        self.disconnect_button.setMinimumWidth(120)
        self.disconnect_button.clicked.connect(self.disconnect_button_clicked)
        h_box_layout_2 = QHBoxLayout()
        h_box_layout_2.addWidget(self.label_1)
        h_box_layout_2.addWidget(self.device_name)
        h_box_layout_1 = QHBoxLayout()
        h_box_layout_1.addWidget(self.connect_button)
        h_box_layout_1.addWidget(self.disconnect_button)
        v_box_layout_3 = QVBoxLayout()
        v_box_layout_3.addLayout(h_box_layout_2)
        v_box_layout_3.addLayout(h_box_layout_1)
        group_box_1.setLayout(v_box_layout_3)

        group_box_2 = QGroupBox("System Parameters")
        group_box_2.setAlignment(Qt.AlignCenter)
        self.label_2 = QLabel("Load Cell Reading [mV]:", self)
        self.label_2.setAlignment(Qt.AlignLeft)
        self.label_2.setFixedWidth(240)
        self.adc_read = QLineEdit("0", self)
        self.adc_read.setDisabled(True)
        self.adc_read.setAlignment(Qt.AlignRight)
        self.adc_read.setFixedWidth(100)
        h_box_layout_3 = QHBoxLayout()
        h_box_layout_3.addWidget(self.label_2)
        h_box_layout_3.addStretch()
        h_box_layout_3.addWidget(self.adc_read)
        self.label_3 = QLabel("Time [ms]:", self)
        self.label_3.setAlignment(Qt.AlignLeft)
        self.label_3.setFixedWidth(240)
        self.time = QLineEdit("0", self)
        self.time.setDisabled(True)
        self.time.setAlignment(Qt.AlignRight)
        self.time.setFixedWidth(100)
        h_box_layout_4 = QHBoxLayout()
        h_box_layout_4.addWidget(self.label_3)
        h_box_layout_4.addStretch()
        h_box_layout_4.addWidget(self.time)
        self.label_4 = QLabel("Set Point:")
        self.label_4.setAlignment(Qt.AlignLeft)
        self.label_4.setFixedWidth(240)
        self.spin_box_1 = QSpinBox(self)
        self.spin_box_1.setFixedWidth(100)
        self.spin_box_1.setMinimum(self.set_point_min)
        self.spin_box_1.setMaximum(self.set_point_max)
        self.spin_box_1.setValue(self.current_set_point)
        self.spin_box_1.valueChanged.connect(self.spin_box_1_value_changed)
        h_box_layout_5 = QHBoxLayout()
        h_box_layout_5.addWidget(self.label_4)
        h_box_layout_5.addStretch()
        h_box_layout_5.addWidget(self.spin_box_1)
        v_box_layout_2 = QVBoxLayout()
        v_box_layout_2.addLayout(h_box_layout_3)
        v_box_layout_2.addLayout(h_box_layout_4)
        v_box_layout_2.addLayout(h_box_layout_5)
        self.dial_1 = QDial(self)
        self.dial_1.setMinimum(self.set_point_min)
        self.dial_1.setMaximum(self.set_point_max)
        self.dial_1.setValue(self.current_set_point)
        self.dial_1.valueChanged.connect(self.dial_1_value_changed)
        h_box_layout_6 = QHBoxLayout()
        h_box_layout_6.addLayout(v_box_layout_2)
        h_box_layout_6.addStretch()
        h_box_layout_6.addWidget(self.dial_1)
        group_box_2.setLayout(h_box_layout_6)

        self.about_btn = QPushButton("About", self)
        self.about_btn.setMinimumWidth(100)
        self.about_btn.clicked.connect(self.about_btn_clicked)

        h_box_layout_7 = QHBoxLayout()
        h_box_layout_7.addWidget(self.about_btn)
        h_box_layout_7.addStretch()

        self.text_edit_1 = QTextEdit(self)
        v_box_layout_4.addWidget(group_box_1)
        v_box_layout_4.addWidget(group_box_2)
        v_box_layout_4.addWidget(self.text_edit_1)
        v_box_layout_4.addLayout(h_box_layout_7)

        self.save_data_button = QPushButton("Export ydata", self)
        self.save_data_button.clicked.connect(self.save_data_button_clicked)

        group_box_3 = QGroupBox("Real-Time Plot", self)
        group_box_3.setAlignment(Qt.AlignCenter)

        self.canvas = Canvas(self)
        self.canvas.setpoint = self.current_set_point
        self.toolbar = NavigationToolbar2QT(self.canvas, self)
        v_box_layout_5 = QVBoxLayout()
        v_box_layout_5.addWidget(self.canvas)
        v_box_layout_5.addWidget(self.toolbar)
        v_box_layout_5.addWidget(self.save_data_button)
        group_box_3.setLayout(v_box_layout_5)

        main_window_layout.addLayout(v_box_layout_4)
        main_window_layout.addWidget(group_box_3)

        self.setLayout(main_window_layout)

        self.serial = QSerialPort()
        self.serial.readyRead.connect(self.serial_ready_read)
        self.serial.portName
        self.timer = QTimer()
        self.timer.setInterval(2000)
        self.timer.start()
        self.timer.timeout.connect(self.timer_period_elapsed)
        self.text_edit_1.append("WELCOME!\n")
        self.text_edit_1.append('Click "About" button to see author information.')
        self.text_edit_1.append('Plug the device and press "Connect".')

    @pyqtSlot()
    def connect_button_clicked(self):
        self.serial.setPortName(self.device_name.text())
        self.serial.setBaudRate(QSerialPort.Baud115200)
        self.serial.setDataBits(QSerialPort.Data8)

        if self.serial.open(QSerialPort.ReadWrite):
            self.text_edit_1.append(
                "Successfully connected to {}".format(self.serial.portName())
            )
            self.setWindowTitle(self.window_title + " - " + self.serial.portName())
        else:
            self.serial_port_error_handler(self.serial.error())

    @pyqtSlot()
    def about_btn_clicked(self):
        self.about_msg_box.exec()

    @pyqtSlot()
    def disconnect_button_clicked(self):
        self.serial.close()
        serial_port_error_code = self.serial.error()
        if serial_port_error_code == QSerialPort.NoError:
            self.text_edit_1.append("Disconnected")
            self.setWindowTitle(self.window_title + " - " + "Disconnected")
        else:
            self.serial_port_error_handler(serial_port_error_code)

    @pyqtSlot()
    def serial_ready_read(self):
        # Read until the first '\n' character
        received_data_string = str(self.serial.readLine(), "utf-8")
        self.handle_serial_data(received_data_string)

    @pyqtSlot()
    def timer_period_elapsed(self):
        # Update controller parameters
        if not self.settings_updated:
            self.send_serial_data("$,{},".format(self.current_set_point))
            self.settings_updated = True

    @pyqtSlot(int)
    def dial_1_value_changed(self, value):
        self.spin_box_1.setValue(value)
        self.current_set_point = value
        self.canvas.setpoint = value
        self.settings_updated = False

    @pyqtSlot(int)
    def spin_box_1_value_changed(self, value):
        self.dial_1.setValue(value)
        self.canvas.setpoint = value

        self.current_set_point = value
        self.settings_updated = False

    @pyqtSlot()
    def save_data_button_clicked(self):
        with open("ydata.bin", "wb") as f:
            p = Pickler(f, protocol=3)
            p.dump(self.saved_data)
        self.text_edit_1.append("'ydata.bin' written!")

    @pyqtSlot()
    def dialog_closed(self):
        # Close open connections and save settings
        try:
            self.serial.close()
        finally:
            self.app_settings.setValue("App/DefaultSerialPort", self.device_name.text())
            self.app_settings.setValue("App/CurrentSetPoint", self.current_set_point)

    def handle_serial_data(self, string="None"):
        fields = string.split(",")
        if len(fields) >= 3 and fields[0] == "$DATA":
            time = int(fields[1])  # [ms]
            adc_reading = int(fields[2])
            self.adc_read.setText(adc_reading_to_mv(adc_reading))
            self.canvas.ydata.append(adc_reading - _LOAD_CELL_OFFSET)
            self.saved_data.append(adc_reading - _LOAD_CELL_OFFSET)
            self.time.setText(str(time))
        else:
            pass

    def send_serial_data(self, msg="None"):
        print(msg)
        msg += "\r"
        self.serial.write(msg.encode("utf-8"))

    def serial_port_error_handler(self, serial_port_error):
        self.setWindowTitle(self.window_title + " - " + "Disconnected")
        if serial_port_error == QSerialPort.DeviceNotFoundError:
            self.text_edit_1.append("Error: Device not found")
        elif serial_port_error == QSerialPort.PermissionError:
            self.text_edit_1.append("Error: Not enough permission")
        elif serial_port_error == QSerialPort.OpenError:
            self.text_edit_1.append("Error: Port already open")
        elif serial_port_error == QSerialPort.NotOpenError:
            self.text_edit_1.append("Error: Device not open")
        elif serial_port_error == QSerialPort.ResourceError:
            self.text_edit_1.append("Device unexpectedly removed from the system")
        else:
            self.text_edit_1.append("An unidentified error occurred")


class Canvas(FingureCanvas):
    setpoint = 0

    def init(self):  # only required for blitting to give a clean slate.
        self.line.set_ydata([np.nan] * len(self.tdata))
        return (self.line,)

    def animate(self, frame):
        # self.line.set_ydata(np.sin(self.x + i / 100))  # update the data.
        # t = self.tdata[-1] + self.dt
        self.line.set_ydata(self.ydata)  # update the data.
        # self.line2.set_ydata(self.ydata2)
        self.hl = self.ax.hlines(
            self.setpoint, 0, self.maxt, color="#ff0000", linestyle="--"
        )
        return (self.line, self.hl)

    def __init__(self, parent=None):
        self.fig = Figure()
        self.ax = self.fig.subplots()
        # self.ax = Axes()
        self.dt = 1 / 80
        self.maxt = 2
        self.tdata = np.arange(0, self.maxt, self.dt)
        self.ydata = deque([0] * len(self.tdata), len(self.tdata))
        self.ax.set_xlim(0, self.maxt)
        self.ax.grid(linestyle="--")
        self.ax.set_ylim(-350000, 350000)

        super().__init__(self.fig)
        self.setParent(parent)
        self.line = Line2D(self.tdata, self.ydata, color="#ffaa00", linewidth="2.5")
        self.ax.add_line(self.line)
        self.ax.set_xlabel("t [s]")

        self.ani = animation.FuncAnimation(
            self.fig,
            self.animate,
            # init_func=self.init,
            frames=len(self.tdata),
            interval=12,
            blit=True,
            save_count=50,
        )


if __name__ == "__main__":
    a = QApplication(sys.argv)
    ui = Ui()
    ui.show()
    sys.exit(a.exec_())