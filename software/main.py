# -*- coding: utf-8 -*-
# main.py -- put your code here!

"""MIT License

Copyright (c) 2019 Ángel G.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

""" 
EXTIx GPIO Configuration
    PC4   ------> GPIO_EXTI4

I2C3 GPIO Configuration    
    PC9     ------> I2C3_SDA
    PA8     ------> I2C3_SCL 

TIM5 GPIO Configuration    
    PA3     ------> TIM5_CH4

L298 GPIO Configuration
    PC1     ------> L298_IN1
    PC5     ------> L298_IN2
    PA3     ------> L298_ENA

SD Card GPIO Configuration
    SD_Card CD/DAT3 ------> PB12
    SD_Card DI/CMD  ------> PB15
    SD_Card CLK     ------> PB13
    SD_Card DO/DAT0 ------> PC2
    SD_Card DAT1    ------> N.C.
    SD_Card DAT2    ------> N.C.
"""

import machine
import pyb
import uos as os

# import uasyncio as asyncio
# import asyn
import micropython
from micropython import const
from pyb import delay

micropython.alloc_emergency_exception_buf(100)

# Initialize hardware handles
i2c3 = pyb.I2C(3)

ld4 = pyb.Pin.cpu.D12  # LD4 [Green Led]
ld3 = pyb.Pin.cpu.D13  # LD3 [Orange Led]
ld5 = pyb.Pin.cpu.D14  # LD5 [Red Led]
ld6 = pyb.Pin.cpu.D15  # LD6 [Blue Led]

# Set-up PA3 as AF TIM5_CH4
l298_in1 = pyb.Pin.cpu.C1
l298_in1.init(pyb.Pin.OUT_PP)
l298_in1.low()
l298_in2 = pyb.Pin.cpu.C5
l298_in2.init(pyb.Pin.OUT_PP)
l298_in2.low()
tim5 = pyb.Timer(5, freq=10000)
tim5_ch4 = tim5.channel(4, pyb.Timer.PWM, pin=pyb.Pin.cpu.A3, pulse_width=0)
# PA3 ------> Pin(Pin.cpu.A3, mode=Pin.ALT, af=Pin.AF2_TIM5)


class PID(object):
    """PID This class implements a PID controller for floating-point data types.
    
    A Proportional Integral Derivative (PID) controller is a generic feedback
    control loop mechanism widely used in industrial control systems.

    The 'update_state' method operate on a single sample of data and each call
    to this method returns a single processed value.

    Algorithm:

        y[n] = y[n-1] + A0 * x[n] + A1 * x[n-1] + A2 * x[n-2]
        
        A0 = Kp + Ki + Kd
        A1 = (-Kp ) - (2 * Kd )
        A2 = Kd

    :param kp: Proportional constant, defaults to 0.0
    :type kp: float, optional
    :param ki: Integral constant, defaults to 0.0
    :type ki: float, optional
    :param kd: Derivative constant, defaults to 0.0
    :type kd: float, optional
    """

    def __init__(self, kp=0.0, ki=0.0, kd=0.0):
        self.a0 = kp + ki + kd
        self.a1 = -kp - 2 * kd
        self.a2 = kd

        self.state = [0.0] * 3  # State "list" of length 3
        self.position = 0.0

    def update_state(self, error):
        self.position = (
            self.state[2]
            + self.a0 * error
            + self.a1 * self.state[0]
            + self.a2 * self.state[1]
        )

        self.state[1] = self.state[0]  # u[n-2]
        self.state[0] = error  # u[n-1]
        self.state[2] = self.position  # x[n-1]

        return self.position


# NAU7802 I2C Address
_NAU7802_I2C_ADDR = const(0x2A)

# NAU7802 register map
_PU_CTRL = const(0x00)
_CTRL1 = const(0x01)
_CTRL2 = const(0x02)
_OCAL1_B2 = const(0x03)
_OCAL1_B1 = const(0x04)
_OCAL1_B0 = const(0x05)
_GCAL1_B3 = const(0x06)
_GCAL1_B2 = const(0x07)
_GCAL1_B1 = const(0x08)
_GCAL1_B0 = const(0x09)
_OCAL2_B2 = const(0x0A)
_OCAL2_B1 = const(0x0B)
_OCAL2_B0 = const(0x0C)
_GCAL2_B3 = const(0x0D)
_GCAL2_B2 = const(0x0E)
_GCAL2_B1 = const(0x0F)
_GCAL2_B0 = const(0x10)
_I2C_CTRL = const(0x11)
_ADCO_B2 = const(0x12)
_ADCO_B1 = const(0x13)
_ADCO_B0 = const(0x14)
_ADC_REG = const(0x15)
_OTP_B1 = const(0x15)
_OTP_B0 = const(0x16)
_REVISION = const(0x1F)
_PWR_CTRL = const(0x1C)

# PGA gain select
_PGA_GAIN_128 = const(0b111)
_PGA_GAIN_64 = const(0b110)
_PGA_GAIN_32 = const(0b101)
_PGA_GAIN_16 = const(0b100)
_PGA_GAIN_8 = const(0b011)
_PGA_GAIN_4 = const(0b010)
_PGA_GAIN_2 = const(0b001)
_PGA_GAIN_1 = const(0b000)

# Conversion rate select
_ADC_CR_320SPS = const(0b111)
_ADC_CR_80SPS = const(0b011)
_ADC_CR_40SPS = const(0b010)
_ADC_CR_20SPS = const(0b001)
_ADC_CR_10SPS = const(0b000)

# LDO Voltage select
_LDO_4V5 = const(0b000)
_LDO_4V2 = const(0b001)
_LDO_3V9 = const(0b010)
_LDO_3V6 = const(0b011)
_LDO_3V3 = const(0b100)
_LDO_3V0 = const(0b101)
_LDO_2V7 = const(0b110)
_LDO_2V4 = const(0b111)

_ADC_MIN_VAL = const(-8388608)
_ADC_MAX_VAL = const(8388607)
_ADC_VREF = const(4465)

""" Memory allocation is not allowed during interrupt service routines, we need
to create a pre-allocated mutable buffer to store the ADC conversion result.
"""

xadc_conversion_result = bytearray(3)

# Create a new PID controller instance
pid1 = PID(kp=3.5, ki=0.6, kd=30)

_LOAD_CELL_OFFSET = const(56216)  ## Do not edit this value ##


class SystemParameters:
    setpoint = 180000


def loadcell_drdy_callback(line):
    # ADCO(B3:B0) burst read
    i2c3.send(_ADCO_B2, addr=42)
    i2c3.recv(xadc_conversion_result, addr=42)
    # micropython.schedule(func, arg)
    micropython.schedule(process_handler, None)


def loadcell_reset(i2c):
    i2c.mem_write(0x01, _NAU7802_I2C_ADDR, _PU_CTRL)
    pyb.delay(1)
    i2c.mem_write(0x00, _NAU7802_I2C_ADDR, _PU_CTRL)
    pyb.delay(1)


def loadcell_power_up(i2c):
    i2c.mem_write(0xAE, _NAU7802_I2C_ADDR, _PU_CTRL)
    pyb.delay(1)


def loadcell_set_vldo_gain(i2c, vldo=_LDO_4V5, pga_gain=_PGA_GAIN_1):
    i2c.mem_write(((vldo << 3) | pga_gain), _NAU7802_I2C_ADDR, _CTRL1)


def loadcell_set_rate(i2c, crs=_ADC_CR_10SPS):
    i2c.mem_write((crs << 4), _NAU7802_I2C_ADDR, _CTRL2)


def loadcell_trigger_calibration(i2c):
    # I2C.mem_read(data, addr, memaddr, *, timeout=5000, addr_size=8)
    tmp = i2c.mem_read(1, _NAU7802_I2C_ADDR, _CTRL2)
    i2c.mem_write((tmp[0] | (1 << 2)), _NAU7802_I2C_ADDR, _CTRL2)
    delay(1)


def loadcell_cycle_start(i2c):
    tmp = i2c.mem_read(1, _NAU7802_I2C_ADDR, _PU_CTRL)
    i2c.mem_write((tmp[0] | (1 << 4)), _NAU7802_I2C_ADDR, _PU_CTRL)


def loadcell_read(i2c):
    xadc_output = i2c.mem_read(3, _NAU7802_I2C_ADDR, _ADCO_B2)
    xadc_value_signed = to_signed(
        (xadc_output[0] << 16) + (xadc_output[1] << 8) + xadc_output[2], 24
    )
    return xadc_value_signed


def to_signed(input_value, num_bits=24):
    """to_signed Converts a two's complement value to a signed integer
    
    :param input_value: Two's complement value
    :type input_value: int
    :param num_bits: The number of bits, defaults to 24
    :type num_bits: int, optional
    """
    mask = 2 ** (num_bits - 1)
    out = -(input_value & mask) + (input_value & ~mask)
    return out


def convert_range(starting_min, starting_max, ending_min, ending_max, value):
    """convert_range Converts values within a range to values within another
    range
    
    :param starting_min: Starting range minimum value
    :type starting_min: float
    :param starting_max: Starting range maximum value
    :type starting_max: float
    :param ending_min: Ending range minimum value
    :type ending_min: float
    :param ending_max: Ending range maximum value
    :type ending_max: float
    :param value: Input value
    :type value: float
    """
    scale = (ending_max - ending_min) / (starting_max - starting_min)
    out = ending_min + ((value - starting_min) * scale)
    return out


def l298_set_duty_cycle(speed):
    """l298_set_duty_cycle Sets the motor speed and direction
    
    :param speed: The motor speed, if this value is positive, the motor will
    spin counterclockwise
    :type speed: float
    """
    if speed < 0:
        l298_in1.low()
        l298_in2.high()
    else:
        l298_in1.high()
        l298_in2.low()
    tim5_ch4.pulse_width_percent(abs(speed))


def write_f_value(f):
    """write_f_value Sets the requested force value to the motor according to
    a linear regression model
    
    :param f: Force value, this value is directly proportional to the applied
    force
    :type f: float
    """
    duty = 0.000100679283123232 * abs(f) + 66.7078068585889
    if f < 0:
        l298_set_duty_cycle(-duty)
    elif f == 0:
        l298_set_duty_cycle(0)
    else:
        l298_set_duty_cycle(duty)


def led_toggle(led):
    if led.value() == 0:
        led.high()
    else:
        led.low()


def error_handler():
    while True:
        led_toggle(ld5)
        delay(100)


def process_handler(arg):
    """process_handler Process a single sample of data after each interrupt

    :param arg: none
    :type arg: none
    """
    t = pyb.millis()
    xadc_value_signed = to_signed(
        (xadc_conversion_result[0] << 16)
        + (xadc_conversion_result[1] << 8)
        + xadc_conversion_result[2],
        24,
    )

    feedback = xadc_value_signed - _LOAD_CELL_OFFSET
    error = SystemParameters.setpoint - feedback
    pid_output = pid1.update_state(error)
    write_f_value(pid_output)

    # Send data to PC
    print("$DATA,{},{}".format(t, xadc_value_signed))


if __name__ == "__main__":

    # Set-up hardware I2C
    i2c3.init(pyb.I2C.MASTER, baudrate=100000, gencall=False, dma=False)

    # Check if the NAU7802 is connected
    if _NAU7802_I2C_ADDR not in i2c3.scan():
        error_handler()

    # Set-up the load cell sensor
    loadcell_reset(i2c3)
    loadcell_power_up(i2c3)
    loadcell_set_vldo_gain(i2c3, vldo=_LDO_4V5, pga_gain=_PGA_GAIN_128)
    loadcell_set_rate(i2c3, _ADC_CR_80SPS)
    i2c3.mem_write(0x30, _NAU7802_I2C_ADDR, _ADC_REG)
    i2c3.mem_write(0x80, _NAU7802_I2C_ADDR, _PWR_CTRL)
    loadcell_trigger_calibration(i2c3)
    loadcell_cycle_start(i2c3)

    # Enable external interrupts on `PC4`
    exti4 = pyb.ExtInt(
        pyb.Pin.board.PC4,
        pyb.ExtInt.IRQ_RISING,
        pyb.Pin.PULL_NONE,
        loadcell_drdy_callback,
    )

    l298_set_duty_cycle(0)

    # Process is taken by interrupts, handle user input through `stdin`
    while True:
        input_string = input()
        if input_string.split(",")[0] == "$":
            SystemParameters.setpoint = int(input_string.split(",")[1])
        else:
            pass